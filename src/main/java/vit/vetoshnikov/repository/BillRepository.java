package vit.vetoshnikov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vit.vetoshnikov.entity.Bill;

@Repository
public interface BillRepository extends JpaRepository<Bill, String> {
}
