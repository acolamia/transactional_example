package vit.vetoshnikov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vit.vetoshnikov.entity.Bill;
import vit.vetoshnikov.enums.StatusResponseEnum;
import vit.vetoshnikov.repository.BillRepository;

import java.util.Collection;

@Service
public class BillService {
    private BillRepository billRepository;

    @Autowired
    public BillService(BillRepository billRepository) {
        this.billRepository = billRepository;
    }

    public StatusResponseEnum save(final Bill bill) {
        if (bill == null) return StatusResponseEnum.INVALID_VALUE;
        billRepository.save(bill);
        return StatusResponseEnum.SUCCESS;
    }

    public Collection<Bill> findAll() {
        return billRepository.findAll();
    }

    public StatusResponseEnum depositMoney(String billId, Long money) {
        if (money <= 0 || money == null) return StatusResponseEnum.INVALID_VALUE;
        final Bill modificatedBill = billRepository.findById(billId).get();
        if (modificatedBill == null) return StatusResponseEnum.THERE_IS_NO_SUCH_USER;
        modificatedBill.setAccountBalance(modificatedBill.getAccountBalance() + money);
        billRepository.save(modificatedBill);
        return StatusResponseEnum.SUCCESS;
    }

    public StatusResponseEnum withdrawMoney(String billId, Long money) {
        if (money <= 0 || money == null) return StatusResponseEnum.INVALID_VALUE;
        final Bill modificatedBill = billRepository.findById(billId).get();
        if (modificatedBill == null) return StatusResponseEnum.THERE_IS_NO_SUCH_USER;
        if (modificatedBill.getAccountBalance()<money)return StatusResponseEnum.NOT_ENOUGH_MONEY;
        modificatedBill.setAccountBalance(modificatedBill.getAccountBalance() - money);
        billRepository.save(modificatedBill);
        return StatusResponseEnum.SUCCESS;
    }
}
